package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId,String start, String end) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isBlank(start)) {
				start = start + " 00:00:00";
			}
			else {
				start = "2020/01/01 00:00:00";
			}

			if(!StringUtils.isBlank(end)) {
				end = end + " 23:59:59";
			}
			else {
				//今日の日付を取得
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Calendar calendar = Calendar.getInstance();
				end = sdFormat.format(calendar.getTime());
			}

			/*
			 * id初期化
			 */
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			/*
			 * idがnullならば全て取得
			 * それ以外ならばidに対応するユーザーidの投稿を取得する
			 */
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM,start,end);
			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(String messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			id = Integer.parseInt(messageId);
			/*
			 * idに対応する投稿を削除する
			 */
			new MessageDao().delete(connection, id);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message select(String messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			/*
			 * idがnullならば全て取得
			 * それ以外ならばidに対応するユーザーidの投稿を取得する
			 */
			Integer id = null;
			id = Integer.parseInt(messageId);
			Message messages = new MessageDao().select(connection, id);
			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
